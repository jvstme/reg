from enum import Enum


class Category(str, Enum):
    hobby_solo = "hobby-solo"
    hobby_couples = "hobby-couples"
    e_st = "e-st"
    d_st = "d-st"
    cb_st = "cb-st"
    open_st = "open-st"
    e_la = "e-la"
    d_la = "d-la"
    cb_la = "cb-la"
    open_la = "open-la"

    def is_solo(self):
        return self == Category.hobby_solo

    def to_human_readable(self):
        return {
            "hobby-solo": "Хобби Соло",
            "hobby-couples": "Хобби Пары",
            "e-st": "E класс Стандарт",
            "d-st": "D класс Стандарт",
            "cb-st": "C+B класс Стандарт",
            "open-st": "Открытый класс Стандарт",
            "e-la": "E класс Латина",
            "d-la": "D класс Латина",
            "cb-la": "C+B класс Латина",
            "open-la": "Открытый класс Латина",
        }[self]

    def incompatible(self):
        incompat = {
            "hobby-solo": [
                "hobby-couples",
                "e-st",
                "d-st",
                "cb-st",
                "open-st",
                "e-la",
                "d-la",
                "cb-la",
                "open-la",
            ],
            "hobby-couples": [
                "hobby-solo",
                "e-st",
                "d-st",
                "cb-st",
                "open-st",
                "e-la",
                "d-la",
                "cb-la",
                "open-la",
            ],
            "e-st": ["hobby-solo", "hobby-couples", "d-st", "cb-st", "open-st"],
            "d-st": ["hobby-solo", "hobby-couples", "e-st", "cb-st", "open-st"],
            "cb-st": ["hobby-solo", "hobby-couples", "e-st", "d-st", "open-st"],
            "open-st": ["hobby-solo", "hobby-couples", "e-st", "d-st", "cb-st"],
            "e-la": ["hobby-solo", "hobby-couples", "d-la", "cb-la", "open-la"],
            "d-la": ["hobby-solo", "hobby-couples", "e-la", "cb-la", "open-la"],
            "cb-la": ["hobby-solo", "hobby-couples", "e-la", "d-la", "open-la"],
            "open-la": ["hobby-solo", "hobby-couples", "e-la", "d-la", "cb-la"],
        }[self]
        return {Category(c) for c in incompat}
