const incompatible = {
    "hobby-solo": ["hobby-couples", "e-st", "d-st", "cb-st", "open-st", "e-la", "d-la", "cb-la", "open-la"],
    "hobby-couples": ["hobby-solo", "e-st", "d-st", "cb-st", "open-st", "e-la", "d-la", "cb-la", "open-la"],
    "e-st": ["hobby-solo", "hobby-couples", "d-st", "cb-st", "open-st"],
    "d-st": ["hobby-solo", "hobby-couples", "e-st", "cb-st", "open-st"],
    "cb-st": ["hobby-solo", "hobby-couples", "e-st", "d-st", "open-st"],
    "open-st": ["hobby-solo", "hobby-couples", "e-st", "d-st", "cb-st"],
    "e-la": ["hobby-solo", "hobby-couples", "d-la", "cb-la", "open-la"],
    "d-la": ["hobby-solo", "hobby-couples", "e-la", "cb-la", "open-la"],
    "cb-la": ["hobby-solo", "hobby-couples", "e-la", "d-la", "open-la"],
    "open-la": ["hobby-solo", "hobby-couples", "e-la", "d-la", "cb-la"],
};

let namesDescr;
let leaderLabel;
let followerInput;
let followerQuestion;
let followerMarker;

let boxes = [];
let soloBox;

window.addEventListener("DOMContentLoaded", function() {
    namesDescr = document.getElementById("descr-names");
    leaderLabel = document.getElementById("label-leader");
    followerInput = document.getElementById("follower");
    followerQuestion = document.getElementById("question-follower");
    followerMarker = document.getElementById("follower-required-marker")

    soloBox = document.getElementById("hobby-solo");
    soloBox.addEventListener("change", handleSoloBox);

    Object.keys(incompatible).forEach(function(boxId) {
        const box = document.getElementById(boxId);
        boxes.push(box);
        box.addEventListener("change", updateDisabled);
    });

    const form = document.getElementById("reg-form");
    form.setAttribute("novalidate", "");
    form.addEventListener("submit", handleSubmit);

    setTimeout(ensureCorrectState, 10);
    setTimeout(ensureCorrectState, 300);
});

function ensureCorrectState() {
    updateDisabled();
    handleSoloBox();
}

function questionId(categoryId) {
    return "question-" + categoryId;
}

function disable(input) {
    input.disabled = true;
    const question = document.getElementById(questionId(input.id));
    question.classList.add("disabled-question");
}

function enable(input) {
    input.disabled = false;
    const question = document.getElementById(questionId(input.id));
    question.classList.remove("disabled-question");
}

function updateDisabled() {
    for (let i = 0; i < boxes.length; i++) {
        enable(boxes[i]);
    }

    for (let i = 0; i < boxes.length; i++) {
        const box = boxes[i];

        if (box.checked) {
            for (let j = 0; j < incompatible[box.id].length; j++) {
                const input = document.getElementById(incompatible[box.id][j]);
                disable(input);
            }
        }
    };
}

function setNamesForCouple() {
    namesDescr.textContent = "Введите фамилию и имя партнёра и партнёрши.";
    leaderLabel.textContent = "Партнёр:";
    followerInput.disabled = false;
    followerInput.required = true;
    followerMarker.textContent = "*";
    followerQuestion.classList.remove("hidden");
}

function setNamesForSolo() {
    namesDescr.textContent = "Введите фамилию и имя солиста.";
    leaderLabel.textContent = "Солист:";
    followerInput.disabled = true;
    followerInput.value = "";
    followerQuestion.classList.add("hidden");
}

function handleSoloBox() {
    if (soloBox.checked) {
        setNamesForSolo();
    } else {
        setNamesForCouple();
    }
}

function highlight(elementId) {
    const el = document.getElementById(elementId);
    el.scrollIntoView({"behavior": "smooth"});

    el.classList.remove("bg-transition");
    el.classList.add("err-highlighted");

    setTimeout(function() {
        el.classList.add("bg-transition");
    }, 1000);
}

function valuePresent(inputId) {
    return !!document.getElementById(inputId).value;
}

function validate() {
    let catSelected = false;

    for (let i = 0; i < boxes.length; i++) {
        if (boxes[i].checked) {
            catSelected = true;
            break;
        }
    }

    if (!catSelected) {
        highlight("section-category");
        return false;
    }

    if (!valuePresent("leader")) {
        highlight("section-names");
        document.getElementById("leader").focus();
        return false;
    }

    if (!soloBox.checked && !valuePresent("follower")) {
        highlight("section-names");
        document.getElementById("follower").focus();
        return false;
    }

    if (!valuePresent("contacts")) {
        highlight("section-contacts");
        document.getElementById("contacts").focus();
        return false;
    }

    if (!valuePresent("club")) {
        highlight("section-club");
        document.getElementById("club").focus();
        return false;
    }

    if (!valuePresent("teacher-1")) {
        highlight("section-teachers");
        document.getElementById("teacher-1").focus();
        return false;
    }

    if (!document.getElementById("consent").checked) {
        highlight("section-consent");
        document.getElementById("consent").focus();
        return false;
    }

    return true;
}

function handleSubmit(e) {
    if (!validate()) {
        e.preventDefault();
    }
}