window.addEventListener("DOMContentLoaded", function() {
    const anchors = document.getElementsByClassName("anchor");
    for (let i = 0; i < anchors.length; i++) {
        anchors[i].addEventListener("click", onAnchorClicked);
    }
});

function onAnchorClicked(e) {
    e.preventDefault();
    const target = e.target || e.srcElement;
    const elId = target.getAttribute("href").split("#")[1];
    highlight(elId);
}

function highlight(elementId) {
    const el = document.getElementById(elementId)
    el.scrollIntoView({"behavior": "smooth"});

    el.classList.remove("bg-transition");
    el.classList.add("info-highlighted");

    setTimeout(function() {
        el.classList.add("bg-transition");
    }, 1000);
}