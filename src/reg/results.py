from collections import OrderedDict
from dataclasses import dataclass

from sqlalchemy.orm import Session

from .database import Entry
from .models import Category


@dataclass
class ResultsRow:
    leader: str
    follower: str
    club: str
    teacher1: str
    teacher2: str


@dataclass
class ResultsSection:
    name_machine: str
    name_human: str
    is_solo: bool
    rows: list[ResultsRow]

    def count_descr(self):
        count = len(self.rows)
        ending = self._count_descr_ending(count)
        return f"{count} заяв{ending}"

    @staticmethod
    def _count_descr_ending(count):
        if count in {11, 12, 13, 14}:
            return "ок"

        digit = count % 10

        if digit == 1:
            return "ка"
        if digit in {2, 3, 4}:
            return "ки"
        return "ок"


def get_results_sections(db: Session):
    sections = OrderedDict(
        [
            (
                cat,
                ResultsSection(
                    cat.value, cat.to_human_readable(), cat.is_solo(), list()
                ),
            )
            for cat in Category
        ]
    )

    query = (
        db.query(Entry)
        .filter(Entry.hide.is_(False))
        .order_by(Entry.leader, Entry.follower)
    )

    for entry in query.all():
        categories = entry.categories.split(",")

        for cat in map(Category, categories):
            row = ResultsRow(
                entry.leader,
                entry.follower,
                entry.club,
                entry.teacher1,
                entry.teacher2,
            )
            sections[cat].rows.append(row)

    return sections.values()
