from .database import Entry, SessionLocal
from .models import Category


def export_entries():
    db = SessionLocal()

    query = db.query(Entry).filter(Entry.hide.is_(False))

    lines = []

    for entry in query.all():
        categories = entry.categories.split(",")

        categories_hr = ", ".join(
            Category(cat).to_human_readable() for cat in categories
        )

        teachers = (
            ", ".join([entry.teacher1, entry.teacher2])
            if entry.teacher2
            else entry.teacher1
        )

        values = [
            entry.date,
            entry.leader,
            entry.follower,
            entry.club,
            teachers,
            entry.contacts,
            categories_hr,
        ]

        line = "\t".join(val if val is not None else "" for val in values)
        lines.append(line)

    print("\n".join(lines))
