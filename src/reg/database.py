from sqlalchemy import Boolean, Column, Integer, String, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from .config import settings

engine = create_engine(settings.db_url, connect_args={"check_same_thread": False})
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


class Entry(Base):
    __tablename__ = "entries"

    id = Column(Integer, primary_key=True)
    date = Column(String)
    leader = Column(String)
    follower = Column(String)
    contacts = Column(String)
    club = Column(String)
    teacher1 = Column(String)
    teacher2 = Column(String)
    categories = Column(String)
    hide = Column(Boolean, default=False)
