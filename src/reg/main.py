import logging
import shutil
from datetime import datetime
from pathlib import Path
from urllib.parse import urlencode

from fastapi import BackgroundTasks, Depends, FastAPI, Form, Request
from fastapi.exceptions import RequestValidationError
from fastapi.responses import FileResponse, RedirectResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from sqlalchemy.orm import Session

from .config import State, settings
from .database import Base, Entry, SessionLocal, engine
from .models import Category
from .results import get_results_sections
from .sheets import TableConnector
from .telegram import TelegramConnector

STATIC_TEMPLATES = ["index.html", "server-error.html", "404.html"]


front_path = Path(__file__).parent.absolute() / "frontend"
static_path = front_path / "static"
templates_path = front_path / "templates"


private_table_connector = TableConnector(
    settings.google_credentials_file,
    settings.private_sheet_id,
    settings.private_sheet_range,
)


telegram_connector = TelegramConnector(
    settings.tg_bot_token,
    settings.tg_chat_id,
)


def validation_error(
    follower: str,
    category: list[Category],
    consent: bool,
):
    if not consent:
        return "Необходимо дать согласие на обработку введённых данных."

    if not category:
        return "Необходимо выбрать хотя бы одну категорию."

    cats = set(category)
    if len(cats) != len(category):
        return "Каждую категорию можно выбрать только один раз."

    for cat in cats:
        if incompatible_cats := cat.incompatible().intersection(cats):
            some_incompatible_cat = next(iter(incompatible_cats))
            return (
                f'Категория "{cat.to_human_readable()}" не совместима с категорией '
                f'"{some_incompatible_cat.to_human_readable()}".'
            )

        if cat.is_solo():
            if follower:
                return (
                    f'Для категории "{cat.to_human_readable()}"'
                    "не нужно вводить ФИО партнёрши."
                )
        else:
            if not follower:
                return "Для выбранных категорий необходимо ввести ФИО партнёрши."


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def clear_dir(path: Path):
    for node in path.iterdir():
        if node.is_dir():
            shutil.rmtree(node)
        else:
            node.unlink()


Base.metadata.create_all(bind=engine)

app = FastAPI(openapi_url=None, root_path=settings.root_path())

templates = Jinja2Templates(directory=templates_path)


@app.on_event("startup")
def build_frontend():
    # rendered_filename: template_filename
    special_templates = dict()

    if settings.state == State.reg_finished:
        special_templates["index.html"] = "index-closed.html"
    elif settings.state == State.comp_finished:
        special_templates["index.html"] = "index-finished.html"

    # TODO: use temporary dirs if FRONTEND_DIST_DIR not specified
    build_dir = settings.frontend_dist_dir
    clear_dir(build_dir)
    shutil.copytree(static_path, build_dir, dirs_exist_ok=True)

    for rendered_filename in STATIC_TEMPLATES:
        template_filename = special_templates.get(rendered_filename, rendered_filename)

        rendered_path = build_dir / rendered_filename
        template = templates.get_template(template_filename)
        content = template.render(
            contact_url=settings.contact_url,
            path_prefix=settings.path_prefix,
        )

        with open(rendered_path, "w") as f:
            f.write(content)


@app.middleware("http")
async def no_cache(request: Request, call_next):
    response = await call_next(request)
    if "Cache-Control" not in response.headers:
        response.headers["Cache-Control"] = "no-cache"
    return response


@app.exception_handler(Exception)
async def exception_handler(req: Request, exc: Exception):
    logging.error("Exception caught in global handler", exc_info=exc)
    return FileResponse(static_path / "server-error.html", status_code=500)


@app.exception_handler(RequestValidationError)
async def validation_error_handler(req: Request, exc: RequestValidationError):
    return templates.TemplateResponse(
        "client-error.html",
        dict(
            contact_url=settings.contact_url,
            path_prefix=settings.path_prefix,
            request=req,
        ),
        status_code=422,
    )


@app.post("/submit")
def submit(
    req: Request,
    bg: BackgroundTasks,
    db: Session = Depends(get_db),
    leader: str = Form(...),
    follower: str = Form(""),
    contacts: str = Form(...),
    club: str = Form(...),
    teacher1: str = Form(...),
    teacher2: str = Form(""),
    category: list[Category] = Form([]),
    consent: bool = Form(...),
):
    if settings.state != State.reg_open:
        return templates.TemplateResponse(
            "closed-error.html",
            dict(
                contact_url=settings.contact_url,
                path_prefix=settings.path_prefix,
                request=req,
            ),
            status_code=400,
        )

    error = validation_error(follower, category, consent)
    if error:
        return templates.TemplateResponse(
            "client-error.html",
            dict(
                error_message=error,
                contact_url=settings.contact_url,
                path_prefix=settings.path_prefix,
                request=req,
            ),
            status_code=422,
        )

    date = datetime.now(settings.timezone()).isoformat(timespec="milliseconds")

    db_entry = Entry(
        date=date,
        leader=leader,
        follower=follower,
        contacts=contacts,
        club=club,
        teacher1=teacher1,
        teacher2=teacher2,
        categories=",".join(category),
        hide=False,
    )

    db.add(db_entry)
    db.commit()

    categories_hr = ", ".join(cat.to_human_readable() for cat in category)
    teachers = ", ".join([teacher1, teacher2]) if teacher2 else teacher1
    participant = f"{leader} - {follower}" if follower else leader

    sheet_values = [date, leader, follower, club, teachers, contacts, categories_hr]
    bg.add_task(private_table_connector.append, sheet_values)

    tg_values = [participant, club, teachers, categories_hr]
    bg.add_task(telegram_connector.notify, tg_values)

    return RedirectResponse(
        "success?" + urlencode(dict(whom=participant, where=categories_hr)),
        303,
    )


@app.get("/success")
async def success(req: Request, whom: str, where: str):
    return templates.TemplateResponse(
        "success.html",
        dict(
            whom=whom,
            where=where,
            contact_url=settings.contact_url,
            path_prefix=settings.path_prefix,
            request=req,
        ),
    )


@app.get("/results")
def results(req: Request, db: Session = Depends(get_db)):
    sections = get_results_sections(db)

    return templates.TemplateResponse(
        "results.html",
        dict(request=req, sections=sections, path_prefix=settings.path_prefix),
    )


@app.get("/rules")
def rules():
    return RedirectResponse(settings.rules_url)


@app.get("/protocols")
def protocols():
    # TODO: handle protocols not available
    return RedirectResponse(settings.protocols_url)


@app.get("/about")
def about():
    return RedirectResponse(settings.about_url)


static_files = StaticFiles(directory=settings.frontend_dist_dir, html=True)
app.mount("/", static_files, name="static")
