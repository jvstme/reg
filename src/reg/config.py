from datetime import timedelta, timezone
from enum import Enum
from pathlib import Path

from pydantic import BaseSettings, HttpUrl, validator


class State(str, Enum):
    reg_open = "reg_open"
    reg_finished = "reg_finished"
    comp_finished = "comp_finished"


class Settings(BaseSettings):
    # Путь до JSON-ключа сервисного аккаунта Google Cloud с доступом к Sheets API
    # Консоль Google Cloud: https://console.cloud.google.com/apis/dashboard
    # Пример настройки проекта на Google Cloud (до 2:35): https://youtu.be/fxGeppjO0Mg
    google_credentials_file: Path

    # ID Google таблицы
    # Можно найти в URL: docs.google.com/spreadsheets/d/<ID>/edit
    # В настройках доступа таблицы необходимо добавить сервисный аккаунт Google Cloud
    private_sheet_id: str

    # С каких ячеек Google таблицы начинать запись
    private_sheet_range: str = "A1"

    # ID Telegram чата для отправки уведомлений
    # Может быть личным чатом пользователя с ботом, либо группой или каналом с ботом
    # Можно найти через https://t.me/get_id_bot
    tg_chat_id: int

    # Токен Telegram бота
    # Создать бота и получить токен: https://t.me/botfather
    tg_bot_token: str

    # Отклонение временной зоны от UTC
    timezone_hours: float = 3

    # Ссылки на группу ВК, положение, протоколы, постер турнира
    contact_url: HttpUrl
    rules_url: str
    protocols_url: str = ""
    about_url: str

    # URL базы данных SQLite3, например sqlite:///./db.sqlite3
    # Файл БД будет создан, если не существует
    db_url: str

    # URL-префикс сайта на веб-сервере
    path_prefix: Path = Path("/")

    # Директория для собранных файлов фронтенда
    # Существующее содержимое директории будет удалено
    frontend_dist_dir: Path

    # Текущее состояние сайта
    state: State = State.reg_open

    class Config:
        env_file = ".env"

    @validator("state")
    def protocols_present_when_comp_finished(cls, state, values):  # noqa: N805
        if state == State.comp_finished and not values["protocols_url"]:
            raise ValueError(
                'protocols_url should be present when state is "comp_finished"',
            )
        return state

    @validator("path_prefix")
    def path_prefix_is_absolute(cls, path_prefix):  # noqa: N805
        if not path_prefix.is_absolute():
            raise ValueError("path_prefix should be absolute")
        return path_prefix

    def root_path(self) -> str:
        return str(self.path_prefix) if self.path_prefix != Path("/") else ""

    def timezone(self) -> timezone:
        return timezone(timedelta(hours=self.timezone_hours))


settings = Settings()
